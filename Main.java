import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            File file = new File("test.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st , binary = "";

            while ((st = br.readLine()) != null) {
                binary += instruction(st);
                System.out.println(instruction(st));
                System.out.println(binaryToHex(binary));
                binary = "";
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
    public static String instruction(String line){
        String[] parts = line.split(" ");
        String inst = parts[0];
        String binary = "";
        if(inst.equalsIgnoreCase("ADD")) {
            binary += "0000";
            binary += convertRegs(parts[1],0);
        }
        else if(inst.equalsIgnoreCase("ADDI")) {
            binary += "0001";
            binary += convertRegs(parts[1],1);
        }
        else if(inst.equalsIgnoreCase("AND")) {
            binary += "0010";
            binary += convertRegs(parts[1],0);
        }
        else if(inst.equalsIgnoreCase("ANDI")) {
            binary += "0011";
            binary += convertRegs(parts[1],1);
        }
        else if(inst.equalsIgnoreCase("OR")) {
            binary += "0100";
            binary += convertRegs(parts[1],0);
        }
        else if(inst.equalsIgnoreCase("ORI")) {
            binary += "0101";
            binary += convertRegs(parts[1],1);
        }
        else if(inst.equalsIgnoreCase("XOR")) {
            binary += "0110";
            binary += convertRegs(parts[1],0);
        }
        else if(inst.equalsIgnoreCase("XORI")) {
            binary += "0111";
            binary += convertRegs(parts[1],1);
        }
        else if(inst.equalsIgnoreCase("JUMP")) {
            binary += "1000";
            binary += jump(parts[1]);
        }
        else if(inst.equalsIgnoreCase("LD")) {
            binary += "1001";
            binary += loadOrStore(parts[1]);
        }
        else if(inst.equalsIgnoreCase("ST")) {
            binary += "1010";
            binary += loadOrStore(parts[1]);
        }
        else if(inst.equalsIgnoreCase("BEQ")) {
            binary += "1011";
            binary += controlİnstruction(parts[1]);
        }
        else if(inst.equalsIgnoreCase("BLT")) {
            binary += "1100";
            binary += controlİnstruction(parts[1]);
        }
        else if(inst.equalsIgnoreCase("BGT")) {
            binary += "1101";
            binary += controlİnstruction(parts[1]);
        }
        else if(inst.equalsIgnoreCase("BLE")) {
            binary += "1110";
            binary += controlİnstruction(parts[1]);
        }
        else if(inst.equalsIgnoreCase("BGE")) {
            binary += "1111";
            binary += controlİnstruction(parts[1]);
        }

        return binary;
    }
    public static String convertRegs(String reg, int type){
        String[] registers = reg.split(",");
        String binary = "";
        int i = 0;
        for (String element : registers){
            if ((type == 1) && (i==2) ){
                int value = Integer.parseInt(element);
                if (value < 0)
                    binary += "1";
                else
                    binary += "0";
                binary += convertBinary(Math.abs(Integer.parseInt(element)),5);
                break;
            }
            binary += convertBinary(Integer.parseInt(element.substring(1)) , 4);
            i++;
        }
        if(type == 0)
            binary += "00";
        return binary;
    }

    public static String jump(String reg){
        String binary = "";
        int value = Integer.parseInt(reg);
        if (value < 0)
            binary += 1;
        else
            binary += 0;

        binary += convertBinary(Math.abs(value),13);
        return binary;
    }
    public static String loadOrStore(String reg){
        String[] regs = reg.split(",");
        String binary = "";

        binary += convertBinary(Integer.parseInt(regs[0].substring(1)) , 4);
        int value = Integer.parseInt(regs[1]);
        if(value < 0)
            binary += 1;
        else
            binary += 0;
        binary += convertBinary(Math.abs(value), 9 );
        return binary;
    }
    public static String controlİnstruction(String reg){
        String binary = "";
        String[] opers = reg.split(",");
        int value = Integer.parseInt(opers[2]);
        for (int i = 0 ; i < 2 ; i++)
            binary += convertBinary(Integer.parseInt(opers[i].substring(1)),4);
        if (value < 0)
            binary += "1";
        else
            binary += "0";

        binary += convertBinary(Math.abs(Integer.parseInt(opers[2])),5);

        return binary;
    }

    public static String convertBinary(int num,int completeness){
        String bin = "" , binary = "";
        while(num > 0){
            bin += num % 2;
            num /= 2;
        }
        for(int i = bin.length() ; i < completeness; i++)
            bin += "0";
        for(int i = bin.length()-1; i >= 0; i--){
            binary += bin.charAt(i);
        }

        return binary;
    }

    private static String binaryToHex(String binary){

        int byteLength = 4;
        int start = 0;
        int position = 0;
        String hex = "";
        int sum = 0;
        int length = binary.length();
        //This if check whether binary is multiple of 4 or not. If not, then fixes it.
        if(length%4 != 0){
            int counter = 0;
            int toErase = length%4;
            while(counter < (byteLength - toErase)){
                binary = "0" + binary;
                counter++;
            }
        }
        length = binary.length(); // update length of binary

        while(start < length){
            //next while divides into groups of four and calculate decimal value of the group.
            while(position < byteLength){
                sum = (int)(sum + Integer.parseInt("" + binary.charAt(length - start - 1)) * Math.pow(2,position));
                position++;
                start++;
            }
            //if decimal value is greater than 10, then the hexadecimal value will be represented alphabetically
            if(sum < 10) hex = Integer.toString(sum) + hex;
            else hex = (char)(sum + 55) + hex;

            position = 0;
            sum = 0;
        }
        return hex;
    }

}
